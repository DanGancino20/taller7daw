function addRow() {
  var tableBody = document.getElementById("tableBody");
  var row = tableBody.insertRow(-1);
  var cell1 = row.insertCell(0);
  var cell2 = row.insertCell(1);
  var cell3 = row.insertCell(2);
  cell1.innerHTML = document.getElementById("input1").value;
  cell2.innerHTML = document.getElementById("input2").value;
  cell3.innerHTML = document.getElementById("input3").value;
 
  document.getElementById("input1").value = "";
  document.getElementById("input2").value = "";
  document.getElementById("input3").value = "";
}
